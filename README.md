# Se requiere instalar #

```
#!python

sudo pip install xmltodic
```


# Otras librerias utilizadas que debiesen ya estar: #
* urllib2
* json

 
# Para utilizar se debe estar en la ruta /mysite y ejecutar #
 
```
#!python

python manage.py runserver

```

# Pendientes #
Se debe crear un segundo servicio en la URL (ej: http:/localhost/futbol) con las siguientes características:

* ~~Crear un nuevo decorador el cual deberá solo hacer la descarga del siguiente XML https://s3-sa- east-1.amazonaws.com/cmpsbtv/matchs.xml ~~

*  Se debe pasar por parámetros la información a la función decorada, y de manera adicional ejecutar el decorador creado anteriormente dentro del decorador actual para así modificar la salida de la función decorada. (NO SE UTILIZA NADA DE LA PREGUNTA 1)

*  ~~La función principal deberá devolver todos los partidos finalizados~~ 

*  ~~Los partidos deben estar ordenados por la fecha en la que jugaron e indicando que equipo gano o si fue empate.~~