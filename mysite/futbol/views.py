from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from functools import wraps

import urllib2
import xmltodict
import json


def check_none(value):
    if(value is None or value == ""):
        result = 'null'
        return result
    else:
        return value

def determinar_datos(partido):
    #Determino quien juega vs quien
    vs = check_none(partido['local']['@nc']) + ' - ' + check_none(partido['visitante']['@nc'])

    #Determino el resultado del partido
    #Si hay aun no se disputa el partido
    if(check_none(partido['goleslocal']) == 'null' or check_none(partido['golesvisitante'])== 'null'):
        resultado = 'A definir'
    else:
        resultado = partido['goleslocal'] + ' - ' + partido['golesvisitante']
    
    #Determino el ganador del partido
    #Si aun no se ha disputado el partido
    if(check_none(partido['goleslocal']) == 'null' or check_none(partido['golesvisitante'])== 'null'):
        ganador= 'A definir'
    #Empate
    elif(partido['goleslocal'] == partido['golesvisitante']):
        ganador= 'Empate'
    #Gana local
    elif(partido['goleslocal'] > partido['golesvisitante']):
        ganador= partido['local']['@nc'] 
    #Gana visitante
    else:
        ganador= partido['visitante']['@nc'] 

    #Determinar fecha
    fecha=  partido['@hora'][0:5] + ' ' + partido['@fecha'][6:8]+'-'+ partido['@fecha'][4:6]+'-'+ partido['@fecha'][0:4]
    
    print partido['@nro'] + ' ' +  vs + ' ' + resultado + ' ' +  ganador + ' ' + fecha
    output= { '@nroa': partido['@nro'], 'partido': vs , 'resultado': resultado , 'ganador': ganador, 'fecha' : fecha}

    return output


def partidos_disputados(dict_input):
    partidos_total= []
    fechas= dict_input['fixture']['fecha']

    for fecha in fechas:

        partidos_por_fecha= fecha['partido']
        #Reviso que los partidos_por_fecha sea lista
        if isinstance(partidos_por_fecha, list):
            for partido in partidos_por_fecha:
                
                if(partido['estado']['@id']== '2'):
                    partidos_total.append(determinar_datos(partido))
        else:
            if(partidos_por_fecha['estado']['@id']== '2'):
                partidos_total.append(determinar_datos(partidos_por_fecha))

    return partidos_total

def download_xml(func):
    @wraps(func)
    def xml_process(request, *args, **kwargs):
        #Lectura y parseo a dictionario del xml
        response = urllib2.urlopen('https://s3-sa-east-1.amazonaws.com/cmpsbtv/matchs.xml')
        data = response.read()
        dataDICT = xmltodict.parse(data)

        #Determino los partidos disputados 
        partidos = partidos_disputados(dataDICT)

        #Preparo salida JSON
        output={ 'partidos' : partidos }
        
        return func(output)
    return xml_process

@download_xml
def index(request):
  return JsonResponse(request, safe=False)

